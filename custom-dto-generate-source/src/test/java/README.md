##Maven Project to generate SalesForce Objects from SalesForce Rest Api and Drop/Create tables in Oracle

## Command to Generate SalesForce Classes ##
**Note**
Replace {version.replace} with real version no. such as 31.0, 32.0

mvn clean generate-sources  -P salesforce-generate-source  -DcamelSalesforce.version={version.replace}


The generated classes are annotated with JPA specific annotations.

##Schema generation and  drop/create tables ##

Modify persistence.xml to have the correct credentials

Find the following xml tags in persistence.xml and replace with correct values
```
			<property name="hibernate.connection.url" value="jdbc:oracle:thin:@localhost:1521:xe" />
			<property name="hibernate.connection.password" value="password99" />
			<property name="hibernate.connection.username" value="chris" />
```
## Drop/Create Tables ##
mvn clean compile exec:java -Dexec.mainClass=PersistenceSetup