import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.hibernate.jpa.HibernateEntityManagerFactory;


public class CreateTriggers {
	public  void create(EntityManagerFactory emf) throws IOException {

		// System.out.println("	Package.getPackages(); "+
		// sma__MATerritoryCollection__c.class.get);
		try {
			
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("trigger.txt");
			byte [] b = new byte[1024];
			StringBuffer sb = new StringBuffer();
			while(stream.read(b, 0, b.length) != -1){
				sb.append(new String(b));
			}
			
			//System.out.println(sb.toString());
		/*	if(true){
				return;
			}*/
			String trig = sb.toString();
			trig = trig.replaceAll("\n", "  ");
			System.out.println("trig : "+trig);
			
			//EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.ofi.salesforce.domain");
			HibernateEntityManagerFactory hibFactory = (HibernateEntityManagerFactory) emf;
			Class[] clazzArr = SomeUtil.getClasses("org.apache.camel.salesforce.dto");
			System.out.println("clazzArr: "+clazzArr.length);
			int ctr = 0;
			for (Class clazz : clazzArr) {
				boolean entityPres  = clazz.isAnnotationPresent(Entity.class);
				if(entityPres){
					Annotation entity = clazz.getAnnotation(Entity.class);
					Entity en = (Entity) entity;
					System.out.println(en.name());
					
					try {
						EntityManager em = emf.createEntityManager();
						em.getTransaction().begin();
						final String repl = trig.replace("$tableName", en.name()).trim();

						Session session = hibFactory.getSessionFactory().openSession();
						session.doWork(new Work() {
							
							public void execute(Connection connection) throws SQLException {
								Statement stm = connection.createStatement();
								stm.executeUpdate(repl);
								
							}
						});
						
						em.getTransaction().commit();
					} catch (Exception e) {
						
						e.printStackTrace();
					}
					ctr ++;
				}
			}
			
			System.out.println("total entities "+ctr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.exit(0);
	}


}
