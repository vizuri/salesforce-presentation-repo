import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;
import org.hibernate.jpa.HibernateEntityManagerFactory;
public class PersistenceSetup {

	public static void main(String[] args) {
		
		try {
			Map<String,String> env = new HashMap<String, String>();
			//System.out.println("Contact : "+Contact.class.getName());
			//EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.ofi.salesforce.domain");
			String username = System.getProperty("db.username");
			if(username != null){
				env.put("hibernate.connection.username", username);
			}
			String password = System.getProperty("db.password");
			if(password != null){
				env.put("hibernate.connection.password",password);
			}
			String dburl = System.getProperty("db.url");
			if(dburl != null){
				env.put("hibernate.connection.url",dburl);
			}
			System.out.println("env properties : "+env);
			/*
			 * 
			 * 	<property name="hibernate.connection.url" value="jdbc:oracle:thin:@localhost:1521:xes" />
			  <property name="hibernate.dialect" value="org.hibernate.dialect.Oracle10gDialect" />
			<property name="hibernate.connection.driver_class" value="oracle.jdbc.OracleDriver" />
			<property name="hibernate.connection.password" value="password99" />
			<property name=""hibernate.connection.password" value="chris" />
			 */
			
			//Persistence.generateSchema("com.ofi.salesforce.domain", env);
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.vizuri.salesforce.demo",env);
			
			
			//Thread.sleep(2000);
			//PersistenceSetup set = new PersistenceSetup();
			//set.create(emf);
			//emf.createEntityManager().createNativeQuery("").executeUpdate()
			emf.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			System.exit(0);
		
	}
	
	private   void create(EntityManagerFactory emf) throws IOException {

		// System.out.println("	Package.getPackages(); "+
		// sma__MATerritoryCollection__c.class.get);
		final StringBuilder trigBuild = new StringBuilder();
		try {
			
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("trigger.txt");
			byte [] b = new byte[1024];
			StringBuffer sb = new StringBuffer();
			while(stream.read(b, 0, b.length) != -1){
				sb.append(new String(b));
			}
			
			//System.out.println(sb.toString());
		/*	if(true){
				return;
			}*/
			String trig = sb.toString();
			trig = trig.replaceAll("\n", "  ");
			
			
			//EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.ofi.salesforce.domain");
			HibernateEntityManagerFactory hibFactory = (HibernateEntityManagerFactory) emf;
			
			Class[] clazzArr = SomeUtil.getClasses("org.apache.camel.salesforce.dto"); 
			System.out.println("clazzArr: "+clazzArr.length);
			int ctr = 0;
			int totalEntitesCreated = 0;
			for (Class clazz : clazzArr) {
				totalEntitesCreated++;
				boolean entityPres  = clazz.isAnnotationPresent(Entity.class);
				if(entityPres){
					Annotation entity = clazz.getAnnotation(Entity.class);
					Entity en = (Entity) entity;
					System.out.println(en.name());
					Session session = null;
					try {
						//EntityManager em = emf.createEntityManager();
						//em.getTransaction().begin();
						String goodTblName = truncateColumn(en.name());
						String trigName = "TRG" +en.name().substring(4, goodTblName.length()).replaceAll("\"", "");
						
						String repl = trig.replace("$tableName", en.name()).trim();
						repl = repl.replace("$trigName", trigName).trim();
						final String trigToExec = repl;
						
						trigBuild.append(trigToExec+"\n");
						
						System.out.println("trigToExec : "+trigToExec);
						session =  hibFactory.getSessionFactory().openSession();
						final Transaction trans = session.beginTransaction();
						session.doWork(new Work() {
							
							public void execute(Connection connection) throws SQLException {
								Statement stm = connection.createStatement();
								
								stm.executeUpdate(trigToExec);
								stm.close();
								trans.commit();
								//connection.close();
								
								
							}
						});
						
						
					} catch (Exception e) {
						
						e.printStackTrace();
						return;
					}finally{
						try {
							if(session.isConnected()){
								session.disconnect();
								
							}
							
						} catch (Exception e) {
							
							//e.printStackTrace();
						}
						
					}
					//Thread.sleep(10);
					ctr ++;
					
					/*if(ctr > 20){
						Thread.sleep(5);
						ctr=0;
					}*/
					/*if((ctr/5) > 10){
						Thread.sleep(30);
					}*/
				}
			}
			
			
			System.out.println("total entities "+totalEntitesCreated);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		System.exit(0);
	}
	
	
	private static int MAX_COL_NAME_SIZE = 30;
	
	static final Set<String> trackEnumDups = new HashSet<String>();
	static final Map<String, FieldLenghtAnomaly> trackColumns = new HashMap<String, FieldLenghtAnomaly>();
	private final Set<String> trackName = new HashSet();
	
	private class FieldLenghtAnomaly {
		String firstColumnName;
		int maxTruncSize = MAX_COL_NAME_SIZE;
		int underScoreNum = 0;

		public String getRightSizeColumnName() {
			/*
			 * if(firstColumnName.length() <=30){ return firstColumnName; }
			 */
			String ret = firstColumnName = firstColumnName.substring(0,
					maxTruncSize);
			maxTruncSize--;
			if (!trackName.contains(ret)) {
				trackName.add(ret);
				System.out.println("trackName not contains : " + ret);
			} else {
				System.out.println("recures : " + ret);
				ret = getRightSizeColumnName();
			}

			System.out.println("returning recurse : " + ret);
			return ret;

		}

		public String getRightSizeEnum() {
			String ret = firstColumnName;
			StringBuilder builder = new StringBuilder();
			builder.append(ret);
			for (int i = 0; i < underScoreNum; i++) {
				builder.append("_");
			}
			ret = builder.toString();
			underScoreNum++;
			return ret;

		}

		@Override
		public String toString() {
			return "TrackingColumnHelp [firstTruncatedColumn="
					+ firstColumnName + ", maxTruncSize=" + maxTruncSize
					+ "]";
		}

	}
	
	public String truncateColumn(String propertyName) {
		System.out.println("truncateColumn field : " + propertyName);
		String retProperty = propertyName;
		if (propertyName != null
				&& propertyName.length() > MAX_COL_NAME_SIZE) {

			retProperty = propertyName.substring(0, MAX_COL_NAME_SIZE);
			System.out.println("len > 30 " + propertyName);
			System.out.println("le <  30 " + retProperty);
			FieldLenghtAnomaly help = trackColumns.get(retProperty);
			if (help == null) {
				help = new FieldLenghtAnomaly();
				help.firstColumnName = retProperty;
				trackColumns.put(retProperty, help);
			}
			System.out.println("TrackingColumnHelp " + help);
			retProperty = help.getRightSizeColumnName();
			System.out.println("retProperty Final : " + retProperty);

		}
		// trackColumns.add(propertyName);
		return retProperty;
	}
	
	
	//public static PoolingDataSource setupDataSource() {
		/*PoolingDataSource pds = new PoolingDataSource();
		pds.setUniqueName("jdbc/ofi-ds");
		pds.setClassName("bitronix.tm.resource.jdbc.lrc.LrcXADataSource");
		pds.setMaxPoolSize(5);
		pds.setAllowLocalTransactions(true);
		pds.getDriverProperties().put("user", "postgres");
		pds.getDriverProperties().put("password", "password99!");
		pds.getDriverProperties().put("url", "jdbc:postgresql://localhost:5432/bpms_demo");
		pds.getDriverPr*perties().put("driverClassName", "org.postgresql.Driver");
		pds.init();*/

	//	EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.ofi.salesforce.domain");

		//return pds;
	//}
}//
