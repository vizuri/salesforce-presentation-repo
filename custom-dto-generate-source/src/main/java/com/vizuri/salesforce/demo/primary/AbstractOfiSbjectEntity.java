package com.vizuri.salesforce.demo.primary;

//import org.joda.time.DateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.camel.component.salesforce.api.dto.AbstractDTOBase;
import org.apache.camel.component.salesforce.api.dto.AbstractSObjectBase;
import org.apache.camel.component.salesforce.api.dto.Attributes;
import org.codehaus.jackson.annotate.JsonProperty;

@MappedSuperclass
public abstract class AbstractOfiSbjectEntity extends AbstractDTOBase{

	//private Long primaryKeyFld;
	
	 // WARNING: these fields have case sensitive names,
    // the field name MUST match the field name used by Salesforce
    // DO NOT change these field names to camel case!!!
	
    private Attributes attributes;
	
	
    private String Id;
    private String OwnerId;
    private Boolean IsDeleted;
    private String Name;
    
    
    private Integer inSalesForce;
    
  
    
    @Temporal(TemporalType.DATE)  
    private Date CreatedDate;
    private String CreatedById;
    
    @Temporal(TemporalType.DATE)  
    private Date LastModifiedDate;

    private String LastModifiedById;
    
    @Temporal(TemporalType.DATE)  
    private Date SystemModstamp;
    
    private String LastActivityDate;
    
    @Temporal(TemporalType.DATE)  
    private Date LastViewedDate;
    
    @Temporal(TemporalType.DATE)  
    private Date LastReferencedDate;

	/*@javax.persistence.Id
	@Column(name = "primaryKeyFld")
	@GeneratedValue
	public Long getPrimaryKeyFld() {
		return primaryKeyFld;
	}

	public void setPrimaryKeyFld(Long primaryKeyFld) {
		this.primaryKeyFld = primaryKeyFld;
	}*/



   

    /**
     * Utility method to clear all system {@link AbstractSObjectBase} fields.
     * <p>Useful when reusing a DTO for a new record, or for update/upsert.</p>
     * <p>This method does not clear {@code Name} to allow updating it, so it must be explicitly set to {@code null} if needed.</p>
     */
    public final void clearBaseFields() {
        attributes = null;
        Id = null;
        OwnerId = null;
        IsDeleted = null;
        CreatedDate = null;
        CreatedById = null;
        LastModifiedDate = null;
        LastModifiedById = null;
        SystemModstamp = null;
        LastActivityDate = null;
    }

    @Transient
    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    
    @javax.persistence.Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
    @JsonProperty("Id")
    @Column
    public String getId() {
        return Id;
    }

    @JsonProperty("Id")
    public void setId(String id) {
        this.Id = id;
    }

    @JsonProperty("OwnerId")
    @Column
    public String getOwnerId() {
        return OwnerId;
    }

    @JsonProperty("OwnerId")
    public void setOwnerId(String ownerId) {
        this.OwnerId = ownerId;
    }

    @JsonProperty("IsDeleted")
    @Column
    public Boolean isIsDeleted() {
        return IsDeleted;
    }

    @JsonProperty("IsDeleted")
    public void setIsDeleted(Boolean isDeleted) {
        this.IsDeleted = isDeleted;
    }

    @JsonProperty("Name")
    @Column(/*columnDefinition="nvarchar2(2000)"*/)
    public String getName() {
        return Name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.Name = name;
    }

    @JsonProperty("CreatedDate")
    @Column(columnDefinition="DATE")
    public Date getCreatedDate() {
        return CreatedDate;
    }

    @JsonProperty("CreatedDate")
    public void setCreatedDate(Date createdDate) {
        this.CreatedDate = createdDate;
    }

    @JsonProperty("CreatedById")
    @Column
    public String getCreatedById() {
        return CreatedById;
    }

    @JsonProperty("CreatedById")
    public void setCreatedById(String createdById) {
        this.CreatedById = createdById;
    }

    @JsonProperty("LastModifiedDate")
    @Column(columnDefinition="DATE")
    public Date getLastModifiedDate() {
        return LastModifiedDate;
    }

    @JsonProperty("LastModifiedDate")
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.LastModifiedDate = lastModifiedDate;
    }

    @JsonProperty("LastModifiedById")
    @Column
    public String getLastModifiedById() {
        return LastModifiedById;
    }

    @JsonProperty("LastModifiedById")
    public void setLastModifiedById(String lastModifiedById) {
        this.LastModifiedById = lastModifiedById;
    }

    @JsonProperty("SystemModstamp")
    @Column(columnDefinition="DATE")
    public Date getSystemModstamp() {
        return SystemModstamp;
    }

    @JsonProperty("SystemModstamp")
    public void setSystemModstamp(Date systemModstamp) {
        this.SystemModstamp = systemModstamp;
    }

    @JsonProperty("LastActivityDate")
    @Column
    public String getLastActivityDate() {
        return LastActivityDate;
    }

    @JsonProperty("LastActivityDate")
    public void setLastActivityDate(String lastActivityDate) {
        this.LastActivityDate = lastActivityDate;
    }

    @JsonProperty("LastViewedDate")
    @Column(columnDefinition="DATE")
    public Date getLastViewedDate() {
        return LastViewedDate;
    }

    @JsonProperty("LastViewedDate")
    public void setLastViewedDate(Date lastViewedDate) {
        LastViewedDate = lastViewedDate;
    }

    @JsonProperty("LastReferencedDate")
    @Column(columnDefinition="DATE")
    public Date getLastReferencedDate() {
        return LastReferencedDate;
    }

    @JsonProperty("LastReferencedDate")
    public void setLastReferencedDate(Date lastReferencedDate) {
        LastReferencedDate = lastReferencedDate;
    }
	
    @Column
    public Integer isInSalesForce(){
    	return inSalesForce;
    }
   
    public void setInSalesForce(Integer inSalesForce){
    	this.inSalesForce = inSalesForce;
    }
	
}
