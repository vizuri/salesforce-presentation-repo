package com.vizuri.salesforce.demo.primary;

import java.io.Serializable;

import javax.persistence.Embeddable;


public class PrimaryKey implements Serializable {
	
	int departmentId;
	long projectId;
}
