package com.vizuri.salesforce.demo;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.camel.component.salesforce.SalesforceComponent;
import org.apache.log4j.Logger;
import org.reflections.Reflections;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
/**
 * An extension SalesforceComponent, 
 * Existing component doesn't resolve the packages correctly
 * 
 * 
 * @author ashakya
 *
 */
public class CustomOfiSalesForceComponent extends SalesforceComponent {
	private static final Logger logger = Logger.getLogger(CustomOfiSalesForceComponent.class);
	
	private String subTypeOfClass;
	
	public String getSubTypeOfClass() {
		return subTypeOfClass;
	}

	public void setSubTypeOfClass(String subTypeOfClass) {
		this.subTypeOfClass = subTypeOfClass;
	}

	
	/**
	 * field subTypeOfClass is provided by bean initializer, 
	 * Uses Reflections api to find the class needed by salesforce component
	 */
	@Override
	protected void doStart() throws Exception {
		logger.info("inside doStart");
		super.doStart();
		
		
		Field classMapField = SalesforceComponent.class.getDeclaredField("classMap");
		classMapField.setAccessible(true);
		
		
		   Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
		   
		  Class<?> subtypeOf = Class.forName(subTypeOfClass);
		   
		for(String pack : this.getPackages()){
			Reflections reflections = new Reflections(pack);
			Set setOfClasses = reflections.getSubTypesOf(subtypeOf);
			
			for (Object  class1 : setOfClasses) {
				Class clz = (Class) class1;
				classMap.put(clz.getSimpleName(),clz);
				classMap.put(clz.getName(),clz);
			}
			
			
		}
		
		classMapField.set(this, classMap);
		
	
	}

}
