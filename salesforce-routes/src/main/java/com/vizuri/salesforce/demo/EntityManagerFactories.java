package com.vizuri.salesforce.demo;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.engine.transaction.jta.platform.internal.BitronixJtaPlatform;
import org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
//@EnableAutoConfiguration
public class EntityManagerFactories {
	
	//@Profile(value="!ofi")
	@Bean(name="ofiEntityManagerFactory")
	
	public EntityManagerFactory entityManagerFactoryLocal(DataSource dataSource,
			JpaVendorAdapter adapter) {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		//factoryBean.setPersistenceXmlLocation("classpath:/META-INF/persistence.xml");
		factoryBean.setPackagesToScan("org.apache.camel.salesforce.dto");
		factoryBean.setJpaVendorAdapter(adapter);
		factoryBean.setDataSource(dataSource);
		factoryBean.setPersistenceUnitName("routesFromUnit");
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("configured", "manually");
		//properties.put("hibernate.transaction.jta.platform",  ;
		factoryBean.setJpaPropertyMap(properties);
		factoryBean.afterPropertiesSet();
		return factoryBean.getObject();
	}
	
/*	//@Bean
	@Primary

	public EntityManagerFactory entityManagerFactoryLocalPrimary(DataSource dataSource,
			JpaVendorAdapter adapter) {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		//factoryBean.setPersistenceXmlLocation("classpath:/META-INF/persistence.xml");
		factoryBean.setPackagesToScan("org.apache.camel.salesforce.dto");
		factoryBean.setJpaVendorAdapter(adapter);
		factoryBean.setDataSource(dataSource);
		factoryBean.setPersistenceUnitName("routesFromUnit");
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("configured", "manually");
		//properties.put("hibernate.transaction.jta.platform",  ;
		factoryBean.setJpaPropertyMap(properties);
		factoryBean.afterPropertiesSet();
		return factoryBean.getObject();
	}*/
	
	/*
	@Profile(value="ofi")
	@Bean(name="ofiEntityManagerFactory")
	
	public EntityManagerFactory entityManagerFactoryOfi(DataSource dataSource,
			JpaVendorAdapter adapter) {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setPersistenceXmlLocation("classpath:/META-INF/ofi-persistence.xml");
		factoryBean.setJpaVendorAdapter(adapter);
		factoryBean.setDataSource(dataSource);
		factoryBean.setPersistenceUnitName("routesFromUnit");
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("configured", "manually");
		properties.put("hibernate.transaction.jta.platform", new NoJtaPlatform());
		factoryBean.setJpaPropertyMap(properties);
		factoryBean.afterPropertiesSet();
		return factoryBean.getObject();
	}*/
}
