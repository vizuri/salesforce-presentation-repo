package com.vizuri.salesforce.demo;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
