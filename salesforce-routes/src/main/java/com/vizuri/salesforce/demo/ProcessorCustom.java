package com.vizuri.salesforce.demo;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ProcessorCustom implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		System.out.println("exchange Body class : "+exchange.getIn().getBody().getClass());
		System.out.println("exchange Body : "+exchange.getIn().getBody());
	/*	Entity en = exchange.getIn().getBody().getClass().getAnnotation(Entity.class);
		System.out.println("entity name : "+en);*/
		
		Map<String,Object> headers = exchange.getIn().getHeaders();
		System.out.println("headers : "+headers);
		
		Map<String,Object> propes = exchange.getProperties();
		System.out.println("propes : "+propes);
		
		
	}

}
