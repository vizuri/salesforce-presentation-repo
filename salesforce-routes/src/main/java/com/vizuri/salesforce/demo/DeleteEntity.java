package com.vizuri.salesforce.demo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.vizuri.salesforce.demo.primary.AbstractOfiSbjectEntity;

public class DeleteEntity implements Processor {

	// @BeanInject("ofiEntityManagerFactory")
	//@Autowired
	//@Qualifier("ofiEntityManagerFactory")
	EntityManagerFactory entityMangageFactory;
	
	public void setEntityMangageFactory(EntityManagerFactory entityMangageFactory) {
		this.entityMangageFactory = entityMangageFactory;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		System.out.println("inside  DeleteEntity");
		EntityManager mgr = null;
		try {
			
			mgr  = entityMangageFactory.createEntityManager();// exchange.getIn().getHeader("CamelEntityManager", EntityManager.class);
			mgr.getTransaction().begin();
			System.out.println("exchange.getIn().getBody()  :"+exchange.getIn().getBody());
			System.out.println("exchange.getIn().getBody().getClass()  :"+exchange.getIn().getBody().getClass().getName());
			AbstractOfiSbjectEntity entity = exchange.getIn().getBody(AbstractOfiSbjectEntity.class);
			
			String query ="delete from " +exchange.getIn().getBody().getClass().getName()+ " where id = '"+entity.getId()+"'";
			System.out.println("delete query : "+query);
			int deleted = mgr.createQuery(query).executeUpdate();
			System.out.println("deleted object :"+deleted );
			
			mgr.getTransaction().commit();
		} catch (Exception e) {
			System.err.println("DeleteEntity error");
			e.printStackTrace();
			mgr.getTransaction().rollback();
			
		}
	}

}
