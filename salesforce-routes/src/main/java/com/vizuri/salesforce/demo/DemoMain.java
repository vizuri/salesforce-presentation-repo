package com.vizuri.salesforce.demo;

import org.springframework.core.env.AbstractEnvironment;


//@Configuration

public class DemoMain {
	public static void main(String[] args) throws Exception {
		String profileNames = System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
		
		System.out.println("profileNames that came in property : "+profileNames);
		if(profileNames == null){
			profileNames = "vizurilocal";
		}
		
		System.out.println("profileNames that was set : "+profileNames);
		
		
		System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, profileNames);
		
	
	//	Main main = new Main();
		try {
		//	DemoSpringBootRouter router = new DemoSpringBootRouter();
			DemoSpringBootRouter.main();
			//FatJarRouter fat = new FatJarRouter();
			//fat.main(args);
			//main.enableHangupSupport();
//			main.setBundleName("MyMainBundle");
//
//			// setup the blueprint file here
//			main.setDescriptors("OSGI-INF/blueprint/blueprint.xml");
//
//			// as we run this test without packing ourselves as bundle, then
//			// include ourselves
//			main.setIncludeSelfAsBundle(true);
			// we support *.xml to find any blueprint xml files

			//main.enableHangupSupport();

			// run for 1 second and then stop automatic
			// main.setDuration(1000);
			
			//main.run();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
			return;
		}

		System.exit(0);
	}

}
