package com.vizuri.salesforce.demo;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.camel.Exchange;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.support.ServiceSupport;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

public class CustomJsonDataFormat extends ServiceSupport implements DataFormat   {

	private static final Logger logger = Logger.getLogger(CustomJsonDataFormat.class);
	private ObjectMapper objectMapper = new ObjectMapper() {
		{
			configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS,
					false);

		}
	};
	
	@Override
	public void marshal(Exchange exchange, Object graph, OutputStream stream)
			throws Exception {
		logger.info("inside marshal now");
		objectMapper.writeValue(stream, graph);
	}

	@Override
	public Object unmarshal(Exchange exchange, InputStream stream)
			throws Exception {
		
		//CamelJacksonUnmarshalType
		logger.info("inside unmarshallType now");
		String unmarshallType = exchange.getIn().getHeader("CamelJacksonUnmarshalType",String.class);
		logger.info("unmarshallType : "+unmarshallType);
		Class<?> clazz = Class.forName(unmarshallType);
		
		return objectMapper.readValue(stream, clazz);
		//return null;
	}

	@Override
	protected void doStart() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void doStop() throws Exception {
		// TODO Auto-generated method stub
		
	}

	
}
