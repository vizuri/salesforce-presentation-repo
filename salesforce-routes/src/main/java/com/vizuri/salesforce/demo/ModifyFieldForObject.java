package com.vizuri.salesforce.demo;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

public class ModifyFieldForObject {


	
	public  void setFieldValue(Object target, final String fieldName, final Object fieldValue){
		/*Map<String,Object> source = new HashMap(){{
			put(fieldName, fieldValue);
		}};*/
		//BeanUtils.copyProperties(source, target);
		System.out.println("inside setFieldValue");
		System.out.println("target : "+target);
		System.out.println("fieldName : "+fieldName);
		System.out.println("fieldValue : "+fieldValue);
		Field field = null;
		try {
			
			
			
			field = target.getClass().getDeclaredField(fieldName);

			doField( field,target,   fieldName,   fieldValue);
		} catch (java.lang.NoSuchFieldException e) {
			System.err.println("first time failed");
			try {
				field = target.getClass().getSuperclass().getDeclaredField(fieldName);
				doField( field,target,   fieldName,   fieldValue);
			} catch (Exception e1) {
				System.err.println("second time failed");
				e1.printStackTrace();
			}
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
    private void doField(Field field, Object target, final String fieldName, final Object fieldValue){
    try {




                    field.setAccessible(true);
                    System.out.println("field.getType() : "+field.getType());

                    if(field.getType().equals(Boolean.class)){
                            System.out.println("field.getType() : boolean");
                            field.set(target, Boolean.valueOf(String.valueOf(fieldValue)));
                    } else
                    if(fieldValue instanceof Number || fieldValue instanceof Integer){
                            System.out.println("field.getType() : Number");
                            field.set(target, Integer.parseInt(String.valueOf(fieldValue)));

                    }else if (field.getType().equals(Integer.class) || field.getType().equals(Number.class)){
                            System.out.println("field.getType() : Integer");
                            field.set(target, Integer.parseInt(String.valueOf(fieldValue)));
                    }
                    else{
                            field.set(target, fieldValue);
                    }

                    field.setAccessible(true);

            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            }
    }
	
	
}
