package com.vizuri.salesforce.demo;

import javax.persistence.EntityManager;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class DisconnectEntity implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		System.out.println("inside  DisconnectEntity");
		try {
			EntityManager mgr  = exchange.getIn().getHeader("CamelEntityManager", EntityManager.class);
			Object entity = exchange.getIn().getBody();
			mgr.detach(entity);
		} catch (Exception e) {
			System.err.println("DisconnectEntity error");
			e.printStackTrace();
		}
	}

}
