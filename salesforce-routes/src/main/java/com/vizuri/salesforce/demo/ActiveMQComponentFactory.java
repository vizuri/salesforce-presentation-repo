package com.vizuri.salesforce.demo;

import javax.jms.ConnectionFactory;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.camel.component.jms.JmsConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActiveMQComponentFactory {

	@Autowired
	ConnectionFactory pooledConnectionFactory;
	

	// Inject the XA aware ConnectionFactory (uses the alias and injects the same as above)
//	@Autowired
//	@Qualifier("xaJmsConnectionFactory")
//	private ConnectionFactory xaConnectionFactory;

	// Inject the non-XA aware ConnectionFactory
//	@Autowired
//	@Qualifier("nonXaJmsConnectionFactory")
//	private ConnectionFactory nonXaConnectionFactory;
	
	@Bean
	public ActiveMQComponent buildComponent() {
		System.out.println("building ActiveMqComponent");
		org.apache.camel.component.jms.JmsConfiguration config = new JmsConfiguration(pooledConnectionFactory);
		ActiveMQComponent componet = new ActiveMQComponent();
		componet.setConfiguration(config);
		return componet;
	}
}
