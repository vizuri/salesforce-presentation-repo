package com.vizuri.salesforce.demo;

import org.apache.camel.spring.boot.FatJarRouter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;


@EnableAutoConfiguration
@SpringBootApplication
@ImportResource(value={"classpath:META-INF/spring/camel-context.xml"})
public class TestFatJarRouter extends FatJarRouter {

	public static void main(String[] args) {
		FatJarRouter.main("classpath:META-INF/spring/camel-context.xml");
	}
	
   /* @Override
    public void configure() throws Exception {
        from("netty4-http:http://0.0.0.0:{{http.port}}").
                setBody().simple("ref:stringBean");
    }

    @Bean
    String stringBean() {
        return "stringBean";
    }*/

}