package com.vizuri.salesforce.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.salesforce.dto.Contact;
import org.apache.camel.spring.SpringCamelContext;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//@Ignore
public class SpringRouteTest extends CamelTestSupport {

	//@Test
	public void testJpqQueryWithParam(){
		
	}
	@Test
	public void testCamelRoute() throws Exception {

		// Define some expectations

		// For now, let's just wait for some messages// TODO Add some expectations here

		// Validate our expectations
		//assertMockEndpointsSatisfied();
		
		Contact c = new Contact();
		c.setId("003g000000cSEpIAAW");
		c.setName("me");
		
	    ///Object someBody = template().requestBody("direct:mouth", "{'id':'123'}");
		// System.out.println("somebody : "+someBody);
		     //  LOG.debug("ExecuteQuery: {}", queryRecords);
		 
		//template().sendBodyAndHeader("direct:mouth", "{\"id\":\"123\"}", "CamelJacksonUnmarshalType", "org.apache.camel.salesforce.dto.Contact");
		//
		//direct:receiveForSaleForceInsert
		template().sendBodyAndHeader("direct:receiveForSaleForceInsert", c, "CamelJacksonUnmarshalType", "org.apache.camel.salesforce.dto.Contact");
//		MeSfContact m = new MeSfContact();
//		m.setId("hello");
//		m.setName("me Name");
//		m.setFirstname("mef");
//		m.setDescription(null);
		//template().sendBody("direct:insert",m);
		
	//direct:query	
		//template().sendBody("direct:query",c);
	}
	
	
	/*@Test
	public void testPushTopicCamel(){
		 MockEndpoint mock = getMockEndpoint("mock:pushTopicForContact");
	        mock.expectedMessageCount(1);
	        // assert expected static headers
	        mock.expectedHeaderReceived("CamelSalesforceTopicName", "pushTopicForContactArun");
	        mock.expectedHeaderReceived("CamelSalesforceChannel", "/topic/pushTopicForContactArun");
	        
	        if(true){
	        	return;
	        }
	}*/

    protected ApplicationContext applicationContext;
    //protected TransactionTemplate transactionTemplate;
    //protected EntityManager entityManager;
    @Override
    protected CamelContext createCamelContext() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
        return SpringCamelContext.springCamelContext(applicationContext);
    }
	
	/*@Override
	protected String getBlueprintDescriptor() {
		return "OSGI-INF/blueprint/blueprint.xml";
	}*/
    
    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {
            public void configure() {
            	JsonDataFormat jd = new JsonDataFormat();
            	jd.setLibrary(JsonLibrary.Jackson);
            	jd.setUnmarshalType(Contact.class);
            	
                from("direct:mouth")
                
                .log("hello mouth ${body}")
                .setHeader("CamelJacksonUnmarshalType").constant("org.apache.camel.salesforce.dto.Contact")
                .unmarshal(jd)
                .log("hello ${body}")
                .to("jpa:org.apache.camel.salesforce.dto.Contact")
                ;
                
                from("direct:insert")
                .to("jpa:com.vizuri.salesforce.demo.two:MeSfContact");
//                
//                from("direct:query")
//                .processRef("#customQueryProcessor")
//                .log("body ${body}");
               /* @Override
                protected void assertURIQueryOption(JpaConsumer jpaConsumer) {
                    assertEquals("select o from " + entityName + " o where o.step = 1", jpaConsumer.getQuery());
                }

                @Override
                protected String getEndpointUri() {
                    return "jpa://" + MultiSteps.class.getName() + "?consumer.query=select o from " + entityName + " o where o.step = 1";
                }*/
               
                
            }
        };
    }

}
