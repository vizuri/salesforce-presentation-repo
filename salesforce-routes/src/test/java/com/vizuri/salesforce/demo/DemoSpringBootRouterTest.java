/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.vizuri.salesforce.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.salesforce.dto.Contact;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.vizuri.salesforce.demo.DemoSpringBootRouter;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoSpringBootRouter.class)
//@WebIntegrationTest(randomPort = true)
@IntegrationTest("spring.main.sources=com.vizuri.salesforce.demo")
@ActiveProfiles("vizurilocal")
//@TestPropertySource("/application-ofi.properties")
public class DemoSpringBootRouterTest extends Assert {

    @EndpointInject(uri = "mock:test")
    MockEndpoint mockEndpoint;
    @Autowired
    CamelContext context;
    
    @Autowired
    ApplicationContext applicationContext;
    
   
    
   // @Test
    public void textContextNull(){
    	assertNotNull(context);
    	
    	//context.createProducerTemplate().sen
    }
    
   // @Test 
    public void testInsertContactToDatabase(){
    	context.createProducerTemplate().requestBody("direct:testGetContact", "0036100000R5gOB");
    }
    
    @Test
    public void testInsertNewContactToSalesForce(){
    	Contact c = new Contact();
    	c.setFirstName("from testing");
    	c.setLastName("from lastname");
    	
    	context.createProducerTemplate().requestBody("direct:testInsertOrUpdate", c);
    }
    
  /*  @Test
    public void shouldProduceMessages() throws InterruptedException {
        mockEndpoint.setExpectedCount(1);
        mockEndpoint.assertIsSatisfied();
    }*/
    
   /* @Test 
    public void testJpaInsertContact(){
    	Contact c = new Contact();
    	c.setId("hasfasf");
    	c.setFirstName("first");
    	context.createProducerTemplate().sendBody("direct:receiveForSaleForceInsert", c);
    	
    	try {
			context.getRoute("pushTopicForContactRoute").getConsumer().start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
*
*/
    
    
}
