Inside this root directory 

1. Package install ofi-salesforce-maven-plugin plugin (customized plugin to annotate with jpa annotations)

    mvn -pl custom-salesforce-maven-plugin clean install

2. generate saleseforce DTO

    replace clientId/clientSecrent/userName/password with your own credentials

	 mvn -pl custom-dto-generate-source  clean generate-sources \
	-DclientId=3MVG9KI2HHAq33RzRppmib_rntfFbmrnBuIcL8kPcMaggpgHdJr0OXI6mrU0mISxTr4Va0UUzJ2yB764WTFop \
	-DclientSecret=148833175722556395 \
	-DuserName=ashakya@gmail.com \
	-Dpassword=1vizuridemo \
	-Psalesforce-generate-source	\
	-DcamelSalesforce.includeObjectFile=./tableInclude.txt

    

3. Create /update tables from generated dtos
	replace the username/password/jdbcurl with the one you would connect to
    
    mvn -pl custom-dto-generate-source clean compile exec:java -Dexec.mainClass=PersistenceSetup \
    -Ddb.username=chris \
    -Ddb.password=password99 \
    -Ddb.url=jdbc:oracle:thin:@localhost:1521:xe

4. Package and run  spring boot application

    a) Compile/Package application
        mvn -pl salesforce-routes clean install -DskipTests
        
    b) Run Application
        ( jar  file is located in salesforce-presentation/salesforce-routes/target/salesforce-routes-0.0.1-SNAPSHOT.jar)
         java -jar salesforce-routes/target/salesforce-routes-1.0.0-SNAPSHOT.jar --spring.profiles.active=vizurilocal
        
        or
        
        mvn -pl salesforce-routes clean package spring-boot:run -Dspring.profiles.active=vizurilocal